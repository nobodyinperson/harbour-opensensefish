# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: harbour-opensensefish
Version: 1.0.1
Release: jolla
Summary: Application to access your OpenSenseMap account
License: GPLv3+
URL: https://gitlab.com/nobodyinperson/harbour-opensensefish
Source: %{name}-%{version}.tar.xz
Packager: Yann Büchau <nobodyinperson@gmx.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: gettext
BuildRequires: inkscape
BuildRequires: gettext
BuildRequires: pandoc
BuildRequires: make
Requires: pyotherside-qml-plugin-python3-qt5 >= 1.4
Requires: libsailfishapp-launcher
Requires: python3-base
Requires: python3-sensemapi >= 0.0.11
Requires: sailfishsilica-qt5
Requires: opensensemap-font
Recommends: python3-matplotlib >= 1.5.3

%description
Access your OpenSenseMap account from your SailfishOS device. This app is still
in early development.

%prep
%setup -q

%install
./configure --prefix=/usr
make
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/qml/*.qml
%{_datadir}/%{name}/python/*.py
%{_datadir}/%{name}/images/*.svg
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/%{name}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Sat Sep 14 2019 Yann Büchau <nobodyinperson@gmx.de> 1.0.0-jolla
- major version bump because I consider the app stable enough
- add Swedish translation
- improve some margins
- show sensor icon in sensor page
* Mon Nov 22 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.10-jolla
- Adjustments to use sensemapi v0.0.11
- More efficient connection session management should result in slightly better
  performance of network-related actions
* Mon Nov 13 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.9-jolla
- Display all times in local time
- Improve sensor measurement plots
- Add option to either use a line or a scatter plot
- Add option to disable automatic x-axis fitting to the data
- Add a 'today' plot time frame
- Display information on the current Page on the cover
- Add some sensible cover actions
* Mon Nov 05 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.8-jolla
- Add a sensor page displaying a graph of measurements
- Recommend matplotlib for installation to enable plots
- Speed up app startup by using threads for loading big Python modules
- Show currently running tasks in PullDownMenus and BusyIndicators
- Show GPS coordinates of a senseBox with higher precision
* Mon Oct 19 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.7-jolla
- Fix bug that caused clicking on any senseBox always opened the same senseBox
* Mon Oct 17 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.6-jolla
- Show time of last upload in senseBoxes page
- Show senseBox ID in senseBoxes page
- Open detailed sensors page when clicked on a senseBox
* Mon Oct 11 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.5-jolla
- Fix bug preventing unchecking TextSwitches in AccountDialog being saved
- Display numbers in localized format
- Add menu entry to clear cache for troubleshooting
* Mon Oct 10 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.4-jolla
- Fix Python import bug
* Mon Oct 10 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.3-jolla
- Add management of multiple accounts
- Add displaying an account's senseBox including latest measurements
* Thu Sep 20 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.2-jolla
- Implement basic login functionality
* Thu Sep 20 2018 Yann Büchau <nobodyinperson@gmx.de> 0.0.1-jolla
- First release without any useful functionality

