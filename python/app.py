# pyotherside
import pyotherside

# system modules
import imp
import os
import re
import logging
import operator
import gettext
import locale
import time
import functools
import collections
import threading

# internal modules
from . import config
from .action import action
from .utils import system_timezone

# external modules

logger = logging.getLogger(__name__)
logging.getLogger("sensemapi.xdg").setLevel(logging.WARNING)
logging.getLogger("sensemapi.cache").setLevel(logging.WARNING)

GLOBAL = collections.defaultdict(dict)


def modifies_accounts(decorated_function):
    def wrapper(*args, **kwargs):
        pyotherside.send("accounts-changing")
        val = decorated_function(*args, **kwargs)
        GLOBAL["app"].write_config()
        pyotherside.send("accounts-changed")
        return val

    return wrapper


def tell_gui_to_revise_account_if_authentication_fails(decorated_function):
    def wrapper(*args, **kwargs):
        from sensemapi.errors import (
            OpenSenseMapAPIInvalidCredentialsError,
            NoCredentialsError,
        )

        try:
            val = decorated_function(*args, **kwargs)
            return val
        except (
            OpenSenseMapAPIInvalidCredentialsError,
            NoCredentialsError,
        ) as e:
            logger.warning(
                "{} failed: {}".format(decorated_function.__name__, e)
            )
            pyotherside.send("account-needs-revision", args[0])
            return False

    return wrapper


def reset_sessions():
    n = 0
    if hasattr(GLOBAL["app"], "accounts"):
        for account in GLOBAL["app"].accounts:
            if hasattr(account, "_session"):
                logger.debug(
                    "Closing and deleting session of account with "
                    "username {}".format(account.username)
                )
                account.session.close()
                del account._session
                n += 1
    return n


@action(description="testing-account")
def test_account(properties):
    import sensemapi
    from sensemapi.errors import OpenSenseMapAPIError

    account = sensemapi.account.SenseMapAccount(
        email=properties.get("email") or None,
        username=properties.get("username") or None,
        password=properties.get("password") or None,
        api=properties.get("api") or None,
    )
    try:
        account.sign_in()
        return True
    except OpenSenseMapAPIError as e:
        logger.warning("Test login failed: {}".format(e))
        return False


def accounts_sorted_by_api():
    sorted_accounts = sorted(GLOBAL["app"].accounts, key=lambda x: x.api or "")
    return list(sorted_accounts)


def accounts_in_gui_order():
    return accounts_sorted_by_api()


@action(description="loading-accounts")
def accounts_in_gui_order_json():
    def correct_datatype(acc):
        for key in ("email", "password", "api", "token", "refresh_token"):
            acc[key] = str(acc[key]) if acc[key] else ""
        for key in ("auth_cache", "cache_password"):
            acc[key] = bool(acc[key])
        return acc

    sorted_accounts = accounts_sorted_by_api()
    sorted_accounts_json = map(
        operator.methodcaller("to_json"), sorted_accounts
    )
    sanitized_accounts_json = map(correct_datatype, sorted_accounts_json)
    return list(sanitized_accounts_json)


def account_backend_index(account):
    return GLOBAL["app"].accounts.index(account)


def account_gui_index(account):
    return accounts_in_gui_order().index(account)


def account_by_backend_index(index):
    return GLOBAL["app"].accounts[index]


def account_by_gui_index(index):
    return accounts_in_gui_order()[index]


def gui2backend_index(gui_index):
    return account_backend_index(account_by_gui_index(gui_index))


def backend2gui_index(backend_index):
    return account_gui_index(account_by_backend_index(backend_index))


def get_current_account_index():
    return (
        backend2gui_index(GLOBAL["app"].current_account_index)
        if GLOBAL["app"].current_account_index is not None
        else -1
    )


@action(description="retrieving-boxes")
@tell_gui_to_revise_account_if_authentication_fails
def get_boxes_of_account_by_gui_index_json(
    gui_index, refresh=False, refresh_if_no_boxes=True
):
    account = account_by_gui_index(gui_index)
    if refresh or (
        refresh_if_no_boxes
        and (
            not all(len(box.sensors) for box in account.boxes)
            or not len(account.boxes)
        )
    ):
        logger.debug("Getting boxes of account")
        account.get_own_boxes()

    def box_to_dict(box):
        d = box.to_json()
        times = list(
            sensor.last_time
            for sensor in box.sensors
            if sensor.last_time is not None
        )
        if times:
            d.update(
                {"last_upload_time": max(times).astimezone(system_timezone())}
            )
        return d

    boxes_json = list(map(box_to_dict, account.boxes))
    return boxes_json


@action(description="retrieving-box")
def get_box_of_account_by_gui_index_json(
    account_gui_index, box_index, refresh=False, refresh_if_no_boxes=True
):
    account = account_by_gui_index(account_gui_index)
    if refresh or (
        refresh_if_no_boxes
        and (
            not all(len(box.sensors) for box in account.boxes)
            or not len(account.boxes)
        )
    ):
        account.get_own_boxes()

    def sensor_to_dict(sensor):
        d = sensor.to_json()
        d["last_time"] = (
            sensor.last_time.astimezone(system_timezone())
            if sensor.last_time
            else sensor.last_time
        )
        return d

    box = account.boxes[box_index]
    box_json = box.to_json()
    box_json["sensors"]["sensors"] = list(map(sensor_to_dict, box.sensors))
    return box_json


def set_current_account_index(gui_index):
    new_index = gui2backend_index(gui_index)
    if new_index != GLOBAL["app"].current_account_index:
        reset_sessions()
    GLOBAL["app"].current_account_index = new_index
    return backend2gui_index(GLOBAL["app"].current_account_index)


@action(description="updating-account")
@modifies_accounts
@tell_gui_to_revise_account_if_authentication_fails
def account_get_details(index):
    account = (
        account_by_gui_index(index)
        if index >= 0
        else account_by_backend_index(index)
    )
    logger.debug("account to get details:\n{}".format(account))
    account.get_details()
    return True


@modifies_accounts
def new_account(properties):
    import sensemapi

    logger.debug("Creating new account from properties {}".format(properties))
    account = sensemapi.account.SenseMapAccount(
        email=properties.get("email") or None,
        username=properties.get("username") or None,
        password=properties.get("password") or None,
        api=properties.get("api") or None,
        auth_cache=GLOBAL["app"].auth_cache
        if properties.get("auth_cache")
        else False,
        cache_password=properties.get("cache_password"),
    )
    GLOBAL["app"].accounts.append(account)
    new_index = accounts_sorted_by_api().index(account)
    logger.debug(
        "New created account (gui index {}):\n{}".format(new_index, account)
    )
    return new_index


@modifies_accounts
def update_account(index, properties):
    logger.debug(
        "Updating account with gui index {} from properties {}".format(
            index, properties
        )
    )
    account = account_by_gui_index(index)
    logger.debug("Account with gui index {}:\n{}".format(index, account))
    with account.pause_caching():
        for prop, val in properties.items():
            logger.debug("Considering property {} = {}".format(prop, val))
            if prop == "auth_cache":
                logger.debug("Skipping {} for now".format(prop))
                continue
            if hasattr(account, prop):
                if val != getattr(account, prop):
                    logger.debug("Setting {} to {}".format(prop, val))
                    setattr(account, prop, val)
                else:
                    logger.debug(
                        "No need to set {} to {}, is already {}".format(
                            prop, val, getattr(account, prop)
                        )
                    )
            else:
                logger.warning(
                    "{} has no property '{}'. Skipping.".format(
                        type(account).__name__
                    )
                )
    if not properties.get("auth_cache"):
        account.delete_cache()
    account.auth_cache = (
        GLOBAL["app"].auth_cache if properties.get("auth_cache") else False
    )


@modifies_accounts
def remove_account(index):
    account = account_by_gui_index(index)
    account.delete_cache()
    GLOBAL["app"].accounts.remove(account)


def set_theme(theme, colors=None, extra=None):
    logger.debug("Setting theme to {}".format(theme))
    GLOBAL["theme"] = theme
    if colors:
        # QT works with #AARRGGBB, matplotlib with #RRGGBBAA, thus we have to
        # move the alpha channel to the end by hand...
        colors = {
            k: (
                re.sub(
                    r"^(#)([a-z0-9]{0,2})([a-z0-9]{6,8})$",
                    r"\1\3\2",
                    v,
                    re.IGNORECASE,
                )
            )
            for k, v in colors.items()
        }
        logger.debug("Setting theme_extra to {}".format(colors))
        GLOBAL["theme_colors"] = {}
        GLOBAL["theme_colors"].update(colors)
    if extra:
        GLOBAL["theme_extra"] = {}
        GLOBAL["theme_extra"].update(extra)


def set_screen(screen):
    logger.debug("Setting screen to {}".format(screen))
    GLOBAL["screen"] = screen


def app_version():
    try:
        return GLOBAL["app"].version
    except AttributeError:
        return "?"


@action(description="loading-app")
def load_app():
    logger.debug("Importing sensemapi")
    imp.acquire_lock()
    import sensemapi

    imp.release_lock()
    logger.debug("sensemapi imported successfully")
    logger.info("Creating SenseMapApplication")
    app = sensemapi.application.SenseMapApplication(
        name=config.PACKAGE_NAME, version=config.VERSION
    )
    GLOBAL["app"] = app
    logger.info("The SenseMapApplication was successfully loaded")
    pyotherside.send("app-loaded")


@action(description="loading-plot-backend")
def load_plot_backend():
    try:
        logger.debug("Importing plot module")
        from . import plot

        logger.info("Plot backend was successfully imported")
        plot.GLOBAL = GLOBAL
        pyotherside.send("plot-backend-available", True)
    except ImportError:
        logger.info("Plot backend is not available")
        pyotherside.send("plot-backend-available", False)


app_load_thread = threading.Thread(
    target=load_app, name="app-loader", daemon=True
)
logger.debug("Loading the application in a background thread")
app_load_thread.start()

plot_backend_load_thread = threading.Thread(
    target=load_plot_backend, name="plot-backend-loader", daemon=True
)
logger.debug("Trying to load plot backend in a background thread")
plot_backend_load_thread.start()

pyotherside.send("imported")
logger.info("The Python backend was successfully imported")
