# system modules
import os
import logging
import functools
import time

# external modules
import pyotherside

logger = logging.getLogger(__name__)

try:
    SLOWDOWN = float(os.environ.get("OPENSENSEFISH_SLOWDOWN", 0))
except (TypeError, ValueError):
    SLOWDOWN = 0


def action(description):
    def decorator(decorated_function):
        @functools.wraps(decorated_function)
        def wrapper(*args, **kwargs):
            pyotherside.send("busy", description)
            val = decorated_function(*args, **kwargs)
            if SLOWDOWN:
                logger.debug(
                    "Artificially waiting {} seconds".format(SLOWDOWN)
                )
                time.sleep(SLOWDOWN)
            pyotherside.send("ready", description)
            return val

        return wrapper

    return decorator
