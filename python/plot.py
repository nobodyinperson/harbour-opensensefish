# pyotherside
import pyotherside

# system modules
import imp
import re
import io
import logging
import itertools
import datetime
import functools
import operator
import random
from urllib.parse import parse_qs, urlsplit

# internal modules
from .action import action
from .utils import system_timezone

logger = logging.getLogger(__name__)

# external modules
try:
    imp.acquire_lock()
    import matplotlib
    from matplotlib.colors import to_rgb
    import matplotlib.style
    import matplotlib.font_manager
    import matplotlib.pyplot as plt
    from matplotlib.dates import (
        DateFormatter,
        AutoDateFormatter,
        AutoDateLocator,
        num2date,
    )
    import cycler

    logger.info("Matplotlib imported successsfully")
except ImportError as e:
    logger.info("Matplotlib is not available: {}".format(e))
    raise
finally:
    imp.release_lock()

GLOBAL = {}


class ImageProvider:
    def __init__(self):
        self.routes = {}
        self.register()

    def route(self, route):
        route_bare = route

        def decorator(decorated_function):
            route_real = re.sub(r"^/*([^/]+)/*$", r"\1", route_bare)
            logger.debug(
                "Registering function {} for route {}".format(
                    decorated_function, repr(route_real)
                )
            )
            self.routes[route_real] = decorated_function
            return decorated_function

        return decorator

    def default_plotter(self, fig, route, params={}):
        ax = fig.add_subplot(1, 1, 1)
        ax.set_ylim(0, 1)
        ax.set_xlim(0, 1)
        ax.set_axis_off()
        ax.patch.set_alpha(GLOBAL["theme_extra"]["highlightBackgroundOpacity"])
        ax.text(
            0.5,
            0.5,
            _("unknown route {}").format(repr(route)),
            horizontalalignment="center",
            verticalalignment="center",
            transform=ax.transAxes,
        )

    def register(self):
        logger.debug("Setting image provider")
        pyotherside.set_image_provider(self)
        pyotherside.send("image-provider-ready", True)

    @action(description="plotting")
    def __call__(self, url, sourceSize):
        split = urlsplit(url)
        route, params = split.path, parse_qs(split.query)
        route = re.sub(r"^/*([^/]+)/*$", r"\1", route)
        logger.debug(
            "Image with id {} and parameters {} at "
            "sourceSize {} requested".format(
                repr(split.path), repr(params), repr(sourceSize)
            )
        )
        plotter = self.routes.get(route, self.default_plotter)
        if plotter:
            logger.debug(
                "Plotter for route {} is {}".format(repr(route), repr(plotter))
            )
        Theme, Colors, Theme_, Screen = (
            GLOBAL["theme"],
            GLOBAL["theme_colors"],
            GLOBAL["theme_extra"],
            GLOBAL["screen"],
        )
        width, height = sourceSize
        if width < 1:
            width = int(float(params.get("width", [width])[-1]))
        if height < 1:
            height = int(float(params.get("height", [height])[-1]))
        with matplotlib_sailfish_style():
            figure_kwargs = {}
            if all(x > 0 for x in (width, height)):
                figure_kwargs.update(
                    figsize=[
                        x / plt.rcParams["figure.dpi"] for x in (width, height)
                    ]
                )
            else:
                logger.warning(
                    "Refusing to create a figure with figsize={}. "
                    "Using default.".format(repr(sourceSize))
                )
            fig = plt.figure(**figure_kwargs)
            plotter(fig, route, params)
            buf = io.BytesIO()
            fig.savefig(buf, facecolor="none")
            size = tuple((fig.get_size_inches() * fig.get_dpi()).astype(int))
            plt.close(fig)
            return bytearray(buf.getvalue()), size, pyotherside.format_data


imageprovider = ImageProvider()


@imageprovider.route("/random_walk")
def random_walk(fig, route, params={}):
    Theme, Colors, Theme_, Screen = (
        GLOBAL["theme"],
        GLOBAL["theme_colors"],
        GLOBAL["theme_extra"],
        GLOBAL["screen"],
    )

    def random_walk(n):
        return list(
            itertools.accumulate(
                (random.random() - 0.5) / 0.5 for i in range(n)
            )
        )

    ax = fig.add_subplot(1, 1, 1)
    n = int(params.get("n", ["100"])[0])
    for i in range(2):
        ax.plot(
            random_walk(n),
            random_walk(n),
            label=_("random walk Nr. {}").format(i + 1),
        )
    ax.tick_params(axis="x", which="both", top=False, bottom=False)
    ax.tick_params(axis="y", which="both", left=False, right=False)
    ax.set_xlabel(_("longitude"))
    ax.set_ylabel(_("latitude"))
    legend = ax.legend()
    for text in legend.get_texts():
        text.set_color(Colors["primaryColor"])
    ax.patch.set_alpha(Theme_["highlightBackgroundOpacity"])


@action(description="retrieving-measurements")
def retrieve_measurements(sensor, *args, **kwargs):
    measurements = sensor.get_measurements(*args, **kwargs)
    times, values = measurements.data["createdAt"], measurements.data["value"]
    if times:
        latest = max(range(len(times)), key=times.__getitem__)
        last_time, last_value = times[latest], values[latest]
        last_time = last_time.astimezone(system_timezone())
        if last_time is not None and last_value is not None:
            logger.debug(
                "Telling gui that sensor {} has measured {} at {}".format(
                    sensor.id, last_value, last_time
                )
            )
            pyotherside.send(
                "latest-sensor-measurement",
                {
                    "id": sensor.id,
                    "last_time": last_time,
                    "last_value": last_value,
                },
            )
    return measurements


@imageprovider.route("/sensor-measurements")
def sensor_measurements(fig, route, params={}):
    app = GLOBAL["app"]
    account = app.accounts[app.current_account_index]
    frame = next(iter(params.get("frame", [])), "month")
    now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
    from_dates = {
        "month": now - datetime.timedelta(days=31),
        "week": now - datetime.timedelta(days=7),
        "day": now - datetime.timedelta(days=1),
        "hour": now - datetime.timedelta(hours=1),
        "today": datetime.datetime(
            now.year, now.month, now.day, tzinfo=datetime.timezone.utc
        ),
    }
    date_formatters = {
        "month": lambda d: DateFormatter("%d %b"),
        "week": lambda d: DateFormatter("%a %H:%M"),
        "day": lambda d: DateFormatter("%H:%M"),
        "hour": lambda d: DateFormatter("%H:%M"),
        "today": lambda d: DateFormatter("%H:%M"),
    }
    date_locators = {}
    box_id = next(iter(params.get("box_id", [])), None)
    sensor_id = next(iter(params.get("sensor_id", [])), None)
    box = account.boxes.by_id.get(box_id)
    sensor = box.sensors.by_id.get(sensor_id)
    measurements = retrieve_measurements(sensor, from_date=from_dates[frame])
    ax = fig.add_subplot(1, 1, 1)
    n_data = len(measurements.data["value"])
    if n_data:
        tz = system_timezone()
        times = list(
            map(lambda dt: dt.astimezone(tz), measurements.data["createdAt"])
        )
        data_timespan = max(times) - min(times)
        logger.debug("Converted time: {}".format(repr(times[0])))
        values = measurements.data["value"]
        if "yes" not in params.get("line", ["yes"]) or n_data <= 1:
            plotfunc = ax.scatter
        else:
            plotfunc = ax.plot
        plotfunc(times, values, label=sensor.title)
        ax.set_ylabel(sensor.unit)
        if "yes" not in params.get("zoom", ["yes"]):
            left = from_dates[frame]
            right = now
            diff = abs(right - left)
            left = left - plt.rcParams["axes.xmargin"] * diff
            right = right + plt.rcParams["axes.xmargin"] * diff
            ax.set_xlim(left=left, right=right)
        axes_timespan = functools.reduce(
            operator.sub, map(num2date, ax.get_xlim())
        )
        logger.debug("Axes timespan: {}".format(axes_timespan))
        ax.set_xlabel(_("local time [{timezone}]").format(timezone=tz))
        locator = date_locators.get(
            frame, lambda d: AutoDateLocator(interval_multiples=True)
        )(axes_timespan)
        locator.tz = tz
        formatter = date_formatters.get(
            frame, lambda d: AutoDateFormatter(locator)
        )(axes_timespan)
        formatter.tz = tz
        logger.debug("Locator: {}".format(locator))
        logger.debug("Formatter: {}".format(formatter))
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
        ax.xaxis_date(tz=tz)
        ax.tick_params(axis="x", which="both", top=False, bottom=False)
        ax.tick_params(axis="y", which="both", left=False, right=False)
        ax.legend()
        ax.set_title(
            _("plot created {time}").format(
                time=now.astimezone(tz).strftime("%A %x %X %Z")
            )
        )
        ax.patch.set_alpha(plt.rcParams["legend.framealpha"])
        fig.autofmt_xdate()
    else:
        ax.set_ylim(0, 1)
        ax.set_xlim(0, 1)
        ax.set_axis_off()
        ax.patch.set_alpha(plt.rcParams["legend.framealpha"])
        ax.text(
            0.5,
            0.5,
            _("no data in the\nselected time frame").format(repr(route)),
            horizontalalignment="center",
            verticalalignment="center",
            transform=ax.transAxes,
        )


def matplotlib_sailfish_style():
    Theme, Colors, Theme_, Screen = (
        GLOBAL["theme"],
        GLOBAL["theme_colors"],
        GLOBAL["theme_extra"],
        GLOBAL["screen"],
    )
    style = {
        "font.weight": "bold",
        "axes.linewidth": 2,
        "axes.grid": True,
        "axes.edgecolor": "none",
        "figure.edgecolor": "none",
        "axes.formatter.useoffset": False,
        "axes.formatter.use_locale": True,
        "legend.edgecolor": "none",
        "legend.borderpad": 0.3,
        "legend.handletextpad": 0.3,
        "legend.labelspacing": 0,
        "grid.linewidth": 1,
        "axes.axisbelow": True,
        "axes.labelweight": "bold",
        "axes.titlesize": "medium",
        "figure.dpi": 75,  # TODO: Hard-coding strange DPI value here
        "savefig.bbox": "tight",  # "tight" messes a bit with the size
        "savefig.format": "png",
        "savefig.transparent": False,
        "lines.linewidth": 4,
        "lines.markersize": 3,
    }
    if Theme_:
        style.update(
            {
                "legend.framealpha": Theme_["highlightBackgroundOpacity"],
                "grid.alpha": Theme_["highlightBackgroundOpacity"],
            }
        )
    if Theme:
        style.update(
            {
                "font.size": Theme.fontSizeTiny,
                "legend.fontsize": Theme.fontSizeTiny,
            }
        )
        available_fonts = set()
        available_fonts.update(
            f.name
            for f in itertools.chain(
                matplotlib.font_manager.fontManager.ttflist,
                matplotlib.font_manager.fontManager.afmlist,
            )
        )
        if Theme.fontFamily in available_fonts:
            style["font.family"] = Theme.fontFamily
        else:
            style["font.family"] = max(
                available_fonts,
                key=lambda font: sum(
                    word.lower() in font.lower()
                    for word in Theme.fontFamily.split()
                ),
            )
            logger.warning(
                "Font {} is not available. Using closest match {}.".format(
                    repr(Theme.fontFamily), repr(style["font.family"])
                )
            )
    if Colors:
        style.update(
            {
                "axes.facecolor": to_rgb(Colors["highlightColor"]),
                "axes.labelcolor": to_rgb(Colors["primaryColor"]),
                "lines.color": to_rgb(Colors["highlightColor"]),
                "text.color": to_rgb(Colors["primaryColor"]),
                "grid.color": to_rgb(Colors["highlightColor"]),
                "xtick.color": to_rgb(Colors["secondaryColor"]),
                "ytick.color": to_rgb(Colors["secondaryColor"]),
                "legend.facecolor": to_rgb(Colors["highlightDimmerColor"]),
                "axes.prop_cycle": cycler.cycler(
                    "color",
                    [
                        to_rgb(Colors["highlightColor"]),
                        to_rgb(Colors["secondaryHighlightColor"]),
                    ],
                ),
            }
        )
    return matplotlib.style.context(style)
