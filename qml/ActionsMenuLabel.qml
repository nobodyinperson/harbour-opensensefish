import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

MenuLabel {
    text: app.actions.map(
        function(s){return(app.available_actions[s]+"...")}
            ).join(", ")
    visible: app.actions.length > 0
}
