import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Dialog {

    id: accountdialog

    property int index: -1
    property bool loading: false
    property string signinsuccess: "untried"

    property var coverContent: CoverContent {
        text: index < 0 ? qsTranslate("cover", "New Account") :
            qsTranslate("cover", "Edit Account")
    }

    function account_data () {
        return {
            "email": userfield.text.indexOf("@")>0 ? userfield.text : "",
            "username": userfield.text.indexOf("@")>0 ? "" : userfield.text,
            "password": passwordfield.text,
            "api": apicombobox.host(),
            "auth_cache": rememberlogin.checked,
            "cache_password": rememberpassword.checked
            }
    }

    function test_sign_in () {
        console.debug("Testing sign in for account "
            + JSON.stringify(account_data()))
        accountdialog.loading = true
        python.call("python.app.test_account", [account_data()],
        function (success) {
            accountdialog.signinsuccess = success ? "success" : "fail"
            accountdialog.loading = false
        })
    }

    canAccept: userfield.text.length > 0 && signinsuccess != "fail"

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        BusyIndicator {
            visible: accountdialog.loading
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: accountdialog.loading
        }

        Column {
            id: column
            spacing: Theme.paddingMedium
            width: parent.width

            DialogHeader {
                id: header
                title: qsTranslate("account-dialog", "Account")
                acceptText: qsTranslate("account-dialog", "Save")
                cancelText: qsTranslate("account-dialog", "Cancel")
            }

            ComboBox {
                width: parent.width
                id: apicombobox
                enabled: !accountdialog.loading
                label: qsTranslate("account-dialog","API")
                description: qsTranslate("account-dialog",
                    "The OpenSenseMap API server to use")

                function host () { return "https://" + value }

                menu: ContextMenu {
                    MenuItem { text: "api.opensensemap.org" }
                    MenuItem { text: "api.testing.opensensemap.org" }
                }
            }

            TextField {
                id: userfield
                label: qsTranslate("account-dialog","Email or Username")
                enabled: !accountdialog.loading
                placeholderText: qsTranslate("account-dialog",
                    "Email or Username")
                width: parent.width

                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: passwordfield.text.length > 0 ?
                    "image://theme/icon-m-enter-accept" :
                    "image://theme/icon-m-enter-next"
                EnterKey.onClicked: {
                    if(passwordfield.text.length <= 0) {
                        passwordfield.focus = true
                    }
                    else {
                        if (accountdialog.signinsuccess == "success") {
                            accountdialog.accept()
                        }
                        else {
                            test_sign_in()
                        }
                    }
                }
            }

            PasswordField {
                id: passwordfield
                label: qsTranslate("account-dialog","Password")
                enabled: !accountdialog.loading
                placeholderText: qsTranslate("account-dialog","Password")
                width: parent.width

                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: userfield.text.length > 0 ?
                    "image://theme/icon-m-enter-accept" :
                    "image://theme/icon-m-enter-next"
                EnterKey.onClicked: {
                    console.debug("password entered")
                    if(userfield.text.length <= 0) {
                        userfield.focus = true
                    }
                    else {
                        if (accountdialog.signinsuccess == "success") {
                            accountdialog.accept()
                        }
                        else {
                            test_sign_in()
                        }
                    }
                }
            }

            Label {
                id: signinstatelabel
                anchors.left: parent.left
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.right: parent.right
                anchors.rightMargin: Theme.horizontalPageMargin
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                visible: accountdialog.signinsuccess != "untried"
                text: accountdialog.signinsuccess == "success" ?
                    qsTranslate("account-dialog", "Sign in was successful") :
                    qsTranslate("account-dialog", "Sign in failed")
                color: accountdialog.signinsuccess == "success" ?
                    Theme.highlightColor : "red"
            }

            Button {
                id: signinbutton
                anchors.horizontalCenter: parent.horizontalCenter
                enabled: !accountdialog.loading &&
                    userfield.text.length > 0 && passwordfield.text.length > 0
                text: qsTranslate("account-dialog", "Test login")
                onClicked: test_sign_in()
            }

            TextSwitch {
                id: rememberlogin
                enabled: !accountdialog.loading
                checked: true
                text: qsTranslate("account-dialog", "Remember login")
                description: qsTranslate("account-dialog",
                    "Stores the API access tokens for later reuse.")
            }

            TextSwitch {
                id: rememberpassword
                enabled: !accountdialog.loading && rememberlogin.checked
                text: qsTranslate("account-dialog", "Remember password")
                description: qsTranslate("account-dialog",
                    "Stores the password for later reuse.") + " " +
                    qsTranslate("account-dialog",
                        "The password will be stored in plain text!")
            }
        }
    }

    onAccepted: {
        accountdialog.loading = true
        if(index < 0) {
            console.debug("Telling Python to create new account "
                +JSON.stringify(account_data()))
            python.call("python.app.new_account", [account_data()],
                function (new_index) {
                    console.debug("Python says the new account has gui index "
                        + new_index)
                    console.debug("The accountdialog has an old index of " +
                        + accountdialog.index)
                    accountdialog.index = new_index
                    accountdialog.loading = false
                    })
            }
        else {
            console.debug("Telling Python to update account "
                +index+":"+JSON.stringify(account_data()))
            python.call("python.app.update_account",
                [index, account_data()],
                function () { accountdialog.loading = false })
            }
    }

    Component.onCompleted: {
        if (index >= 0) {
            accountdialog.loading = true
            python.call('python.app.accounts_in_gui_order_json', [],
                function(accounts) {
                    console.debug("Filling dialog from account: "
                        +JSON.stringify(accounts[index]))
                    userfield.text =
                        accounts[index].username || accounts[index].email || ""
                    passwordfield.text = accounts[index].password || ""
                    rememberlogin.checked =
                        accounts[index].auth_cache ? true:false
                    rememberpassword.checked =
                        accounts[index].cache_password ? true : false
                    if(accounts[index].api.indexOf("api.opensensemap.org")>=0){
                        apicombobox.currentIndex=0
                    }
                    else if(accounts[index].api.indexOf(
                        "api.testing.opensensemap.org")>=0) {
                        apicombobox.currentIndex=1
                    }
                    else {
                        apicombobox.currentIndex=0
                    }
                accountdialog.loading = false
            })
        }
    }
}


