import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: firstpage
    property var coverContent: CoverContent {
        text: qsTranslate("cover", "Welcome")
    }

    SilicaFlickable {
        id: view
        anchors.fill: parent

        PullDownMenu {
            busy: !python.imported
            enabled: python.imported

            MenuItem {
                text: qsTranslate("menu","About")
                onClicked: app.pageStack.push("AboutPage.qml")
            }

            MenuItem {
                text: qsTranslate("menu","Accounts")
                enabled: python.app_loaded
                onClicked: app.pageStack.push("Accounts.qml")
            }

            ActionsMenuLabel {}
        }

        PageHeader {
            id: header
            width: parent.width
            title: qsTranslate("first-page","OpenSenseFish")
        }

        ViewPlaceholder {
            id: viewPlaceholder
            enabled: python.imported
            text: qsTranslate("first-page","Welcome to OpenSenseFish!")
            hintText: qsTranslate("first-page",
            "You can select or configure accounts via the %1 menu.")
                .arg(qsTranslate("menu", "Accounts"))
        }

        BusyIndicator {
            id: busyindicator
            visible: !python.imported
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: !python.imported
        }

        Label {
            id: loadinglabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: busyindicator.top
                bottomMargin: Theme.paddingLarge
            }
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            visible: busyindicator.running
            text: qsTranslate("first-page", "Loading backend")
        }

        // Component.onCompleted: {
        //     python.call('python.app.accounts_in_gui_order_json', [],
        //         function(accounts) {
        //             console.debug("Python sent accounts:\n" +
        //                 JSON.stringify(accounts))
        //             if (accounts.length <= 0) {
        //                 app.pageStack.push("Accounts.qml")
        //             }
        //             else {
        //                 python.call('python.app.get_current_account_index',[],
        //                     function (index) {
        //                         app.pageStack.push("senseBoxesPage.qml",
        //                             {"account_index": index})
        //                     })
        //             }
        //         })
        // }
    }
}


