/* -*- coding: utf-8-unix -*-
 *
 * Copyright (C) 2018 Yann Büchau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: page

    property var coverContent: CoverContent {
        text: qsTranslate("cover", "About OpenSenseFish")
    }

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            enabled: app.actions.length > 0
            ActionsMenuLabel {}
        }

        VerticalScrollDecorator {}

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader {
                id: header
                title: qsTranslate("about-page","About OpenSenseFish")
            }

            Label {
                id: label
                textFormat: Text.RichText
                font.pixelSize: Theme.fontSizeMedium
                text :
                    qsTranslate("about-page",
                    "<b>OpenSenseFish</b> is a SailfishOS application to " +
                    "access the <style>a:link{color: %1;}</style> "+
                    "<a href=\"https://opensensemap.org\">OpenSenseMap</a>." +
                    "<p>" +
                    "This is <b>OpenSenseFish</b> version <b>%2</b>."  +
                    "<p>" +
                    "Source code: " +
                   "<style>a:link{color: %1;}</style>"+
                   "<a href=\"https://gitlab.com/nobodyinperson/" +
                   "harbour-opensensefish\">on GitLab.com</a>"
                    )
                    .arg(Theme.highlightColor)
                    .arg(python.call_sync("python.app.app_version"))
                wrapMode: Text.Wrap
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
                onWidthChanged: label.doLayout()
                onLinkActivated: {
                         Qt.openUrlExternally(link)
                        }
            }
        }

    }

}


