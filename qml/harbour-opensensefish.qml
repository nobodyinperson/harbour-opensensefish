/* -*- coding: utf-8-unix -*-
 *
 * Copyright (C) 2018 Yann Büchau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "."

ApplicationWindow
{
    id: app
    property bool loading: false
    property int last_broken_account_index: -2
    property var icons: OpenSenseMapIcons{ id: osemicons }

    // translation mapping of the available actions
    property var available_actions: {
        "loading-app": qsTranslate("action","loading app"),
        "plotting": qsTranslate("action","plotting"),
        "loading-plot-backend": qsTranslate("action","loading plot backend"),
        "retrieving-box": qsTranslate("action","retrieving senseBox"),
        "retrieving-boxes": qsTranslate("action","retrieving senseBoxes"),
        "updating-account": qsTranslate("action","updating account"),
        "testing-account": qsTranslate("action","testing account"),
        "loading-accounts": qsTranslate("action","loading accounts"),
        "retrieving-measurements":
            qsTranslate("action","retrieving measurements"),
    }
    // stack of currently active actions
    property var actions: new Array()
    // conveniece properties holding oldest/latest translated actions
    property var oldest_action: actions.length>0 ?
        available_actions[actions[0]] : qsTranslate("action", "loading")
    property var latest_action: actions.length>0 ?
        available_actions[actions[actions.length-1]] :
        qsTranslate("action", "loading")

    property var state: ""

    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("Cover.qml")
    Python { id: python }

    onOrientationChanged: {
        if(orientation & Orientation.PortraitMask)
            console.debug("Now in portrait mode")
        else
            console.debug("Now in landscape mode")
    }

    Component.onCompleted: {
        Theme.highlightDimmerColorChanged.connect(
            send_theme_and_screen_to_python)
        python.setHandler("ready", function(task) {
            console.debug("Python is done with action '%1'".arg(task))
            end_action(task)
        })
        python.setHandler("busy", function(task) {
            console.debug("Python is busy with '%1'".arg(task))
            begin_action(task)
        })
        python.setHandler("account-needs-revision", function(index) {
            last_broken_account_index = index
            console.info("Python says account Nr. " +index+ " is outdated");
            if(pageStack.busy) {
                console.debug("pageStack is currently busy, "
                    + "cannot open Account dialog yet.")
                function open_account_dialog_when_ready () {
                    if(!pageStack.busy) {
                        open_account_dialog(
                            {"index": index, "signinsuccess": "fail"})
                        pageStack.busyChanged.disconnect(
                            open_account_dialog_when_ready)
                    }
                }
                pageStack.busyChanged.connect(open_account_dialog_when_ready)
            }
            else {
                open_account_dialog({"index": index, "signinsuccess": "fail"})
            }
        })
        send_theme_and_screen_to_python()
        Qt.application.stateChanged.connect(function(){
            if(Qt.application.state != Qt.applicationActive) {
                console.debug("Application is not active anymore. " +
                    "Resetting sessions.")
                python.call("python.app.reset_sessions", [], function(n){
                    console.debug("Python says %1 sessions were reset".arg(n))
                })
            }
        })
    }

    function begin_action (action) {
        if (actions.indexOf(action) >= 0) {
            console.warn("Action '%1' is already running".arg(action))
        }
        else {
            console.debug("Beginning action '%1'".arg(action))
            actions.push(action)
            actionsChanged()
        }
    }

    function end_action (action) {
        var index = actions.indexOf(action)
        if (index >= 0) {
            console.debug("Ending action '%1'".arg(action))
            actions.splice(index, 1)
            actionsChanged()
        }
    }

    function send_theme_and_screen_to_python() {
        // This is necessary as PyOtherSide cannot convert some types
        var colors = {
            "primaryColor": String(Theme.primaryColor),
            "secondaryColor": String(Theme.secondaryColor),
            "highlightColor": String(Theme.highlightColor),
            "secondaryHighlightColor": String(Theme.secondaryHighlightColor),
            "highlightDimmerColor": String(Theme.highlightDimmerColor),
        }
        var extra = {
            "highlightBackgroundOpacity":
                Theme.highlightBackgroundOpacity,
        }
        console.debug("Sending Theme and colors %1 and extras %2 to Python"
            .arg(JSON.stringify(colors)).arg(JSON.stringify(extra)))
        python.call("python.app.set_theme",
            [Theme, colors, extra],function(){})
        console.debug("Sending Screen to Python")
        python.call("python.app.set_screen",[Screen], function(){})
    }

    function update_account_details(index) {
        python.call("python.app.account_get_details", [index],
            function (success) {
                if (success) {
                    console.debug("Updating worked")
                    app.last_broken_account_index = -2
                }
                app.loading = false
            })
    }

    function open_account_dialog (properties) {
        console.debug("Opening AccountDialog with properties "
            + JSON.stringify(properties || {}))
        var dialog = app.pageStack.push("AccountDialog.qml",
            properties ? properties : {})
        function update_dialog_account() {
            console.debug("The dialog's index was changed to " + dialog.index)
            if (dialog.index >= 0) {
                console.debug("The dialog's index is good, update the account")
                dialog.indexChanged.disconnect(update_dialog_account)
                update_account_details(dialog.index)
            }
        }
        dialog.accepted.connect(function () {
            console.debug("Updating account %1: ".arg(dialog.index))
            if(dialog.index < 0) { // index has not been updated yet
                console.debug("The dialog's account's new index has "
                   +  "not yet been set. We have to wait.")
                dialog.indexChanged.connect(update_dialog_account)
                }
            else {
                console.debug("The dialog has a good index, update it")
                update_dialog_account()
            }
        })
        dialog.rejected.connect(function () {
            app.loading = false
        })
        app.loading = true
    }
}


