import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: accountspage

    property var coverContent: CoverContent {
        text: qsTranslate("cover", "Accounts")
        actionIcon: "image://theme/icon-cover-new"
        action: function() {
            app.activate()
            app.open_account_dialog()
        }
    }

    property var accountsmodel: ListModel {}
    property bool loading: false
    property int current_account_index: -1

    function set_current_account (index) {
        accountspage.loading = true
        console.debug("Setting account Nr. "+index+" as current account")
        python.call("python.app.set_current_account_index", [index],
            function (index) {
                accountspage.current_account_index = index
                accountspage.loading = false
            })
    }

    SilicaListView {
        id: accountlist
        anchors.fill: parent

        VerticalScrollDecorator {}

        PullDownMenu {
            MenuItem {
                text: qsTranslate("accounts-page","Clear Cache")
                onClicked: {
                    accountspage.loading = true
                    console.debug("Telling Python to empty the cache")
                    python.call("python.app.app.cache.delete", [], function (){
                        console.debug("Python cleared the cache")
                        accountspage.loading = false
                        })
                }
            }
            MenuItem {
                text: qsTranslate("accounts-page","Add Account")
                onClicked: { app.open_account_dialog() }
            }
            ActionsMenuLabel {}
        }

        header: PageHeader {
            id: header
            visible: accountsmodel.count > 0
            title: qsTranslate("accounts-page", "Accounts")
            }

        model: accountsmodel

        ViewPlaceholder {
            id: viewPlaceholder
            text: qsTranslate("accounts-page","No configured accounts")
            hintText: qsTranslate("accounts-page",
            "Add an account from the pulldown menu.")
            enabled: accountsmodel.count <= 0
                && !accountspage.loading && !python.busy
        }

        BusyIndicator {
            visible: accountspage.loading
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: accountspage.loading
        }

        section {
            property: "api"
            criteria: ViewSection.FullString
            delegate: SectionHeader {
                    text: section
                }
        }

        delegate: ListItem {
            id: accountitem
            enabled: !accountspage.loading
            contentHeight: accountitemusernamelabel.height
                + accountitememaillabel.height
            ListView.onRemove: animateRemoval(accountitem)

            function remove() {
                remorseAction(qsTranslate("accounts-page",
                    "Delete account %1").arg(model.username||model.email),
                    function () {
                        if(python.call("python.app.remove_account",
                            [model.index], function () {})){
                            // remove account from the gui
                            accountlist.model.remove(model.index)
                        }
                    })
            }

            ListItemLabel {
                id: accountitemusernamelabel
                text: model.username || qsTranslate("accounts-page",
                    "username unknown")
                font.italic: !model.username
                font.bold: model.index == accountspage.current_account_index
                color: model.index == accountspage.current_account_index ?
                    Theme.highlightColor : Theme.primaryColor
                height: implicitHeight + Theme.paddingLarge
                verticalAlignment: Text.AlignBottom
                truncationMode: TruncationMode.Fade
            }

            ListItemLabel {
                id: accountitememaillabel
                anchors.top: accountitemusernamelabel.bottom
                font.pixelSize: Theme.fontSizeExtraSmall
                text: model.email || qsTranslate("accounts-page",
                    "email unknown")
                font.italic: !model.email
                font.bold: model.index == accountspage.current_account_index
                color: model.index == accountspage.current_account_index ?
                    Theme.secondaryHighlightColor : Theme.secondaryColor
                height: implicitHeight + Theme.paddingLarge
                verticalAlignment: Text.AlignTop
                truncationMode: TruncationMode.Fade
            }

            Image {
                id: accountitemloginimage
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: Theme.horizontalPageMargin
                source: app.last_broken_account_index != model.index ?
                    (model.token ? "image://theme/icon-s-installed" : "") :
                    "image://theme/icon-s-high-importance"
            }

            menu: ContextMenu {
                MenuItem {
                    text: qsTranslate("accounts-page", "edit")
                    onClicked: {
                        var account = {
                                "email": model.email,
                                "username": model.username,
                                "password": model.password,
                                "cache": model.cache ? true : false,
                                "cache_password": model.cache_password,
                                }
                        console.debug("Opening account %1: "
                            .arg(model.username||model.email)
                            + JSON.stringify(account))
                        app.open_account_dialog({"index": model.index})
                    }
                }
                MenuItem {
                    text: qsTranslate("accounts-page", "update")
                    onClicked: {
                        console.debug("Updating account %1: "
                            .arg(model.username||model.email))
                        accountspage.loading = true
                        python.call("python.app.account_get_details",
                            [model.index],
                            function (success) {
                                accountspage.loading = false
                            })
                    }
                }
                MenuItem {
                    text: qsTranslate("accounts-page", "remove")
                    onClicked: {
                        console.debug("Removing account %1"
                            .arg(model.username))
                        remove()
                        }
                }
            }

            onClicked: {
                if (app.last_broken_account_index == model.index) {
                    console.debug("Account " + model.username||model.email
                        + " is broken, opening account dialog")
                    app.open_account_dialog({"index": model.index})
                }
                else {
                    accountspage.set_current_account(model.index)
                    app.pageStack.push("senseBoxesPage.qml",
                        {"account_index": model.index})
                }
            }
        }

        Component.onCompleted: {
            populate()
            python.setHandler("accounts-changed", function () {
                console.debug("Python says the accounts changed"); populate()
                })
            python.setHandler("accounts-changing", function() {
                console.debug("Python says accounts are currently changing");
                accountspage.loading = true
                })
        }

        function populate () {
            accountspage.loading = true
            python.call("python.app.get_current_account_index", [],
                function (index) {
                    accountspage.current_account_index = index
                })
            python.call('python.app.accounts_in_gui_order_json', [],
                function(accounts) {
                    console.debug("Python sent accounts:\n" +
                        JSON.stringify(accounts))
                    accountsmodel.clear()
                    for (var i = 0; i < accounts.length; i++) {
                        console.debug("Appending account to model: "
                            +JSON.stringify(accounts[i]))
                        accountsmodel.append(accounts[i])
                    }
                accountspage.loading = false
                })
        }

    }

}


