import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: sensorpage

    property int account_index: -1
    property var sensor
    property var box
    property int sensor_index: -1
    property bool loading: false
    property bool account_broken: app.last_broken_account_index==account_index
    property var coverContent: CoverContent {
        text: (sensor.title?sensor.title+"\n\n":"")
            + (sensor.last_value ? (Number(sensor.last_value).toLocaleString()
                + " " + sensor.unit)
            : qsTranslate("cover", "no measurements"))
        actionIcon: "image://theme/icon-cover-refresh"
        action: function() {plot.reload(true, true)}
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        PullDownMenu {
            MenuItem {
                text: qsTranslate("sensor-page","Refresh")
                onClicked: { plot.reload(true, true) }
            }
            ActionsMenuLabel {}
        }

        BusyIndicator {
            id: busyindicator
            visible: sensorpage.loading | python.busy
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: sensorpage.loading | python.busy
        }

        Label {
            id: loadinglabel
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: busyindicator.top
                bottomMargin: Theme.paddingLarge
            }
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            visible: busyindicator.running
            text: app.latest_action
        }

        Column {
            id: column
            spacing: Theme.paddingMedium
            width: parent.width

            PageHeader {
                id: header
                visible: sensor_index >= 0
                title: (sensor.title||"")  + " " + osemicons.get(sensor.icon)
                description: box.name || ""
            }

            SectionHeader {
                text: sensor.type
            }

            Plot {
                id: plot
            }

            ComboBox {
                id: framecombobox
                width: parent.width
                label: qsTranslate("sensor-page","time frame")
                description: qsTranslate("sensor-page",
                    "for the graph")
                currentIndex: options_order.indexOf("hour")
                enabled: !plot.loading
                property var available_options: {
                    "month" : qsTranslate("sensor-page","month"),
                    "week"  : qsTranslate("sensor-page","week"),
                    "day"   : qsTranslate("sensor-page","day"),
                    "hour"  : qsTranslate("sensor-page","hour"),
                    "today" : qsTranslate("sensor-page","today"),
                    }
                property var options_order:
                    ["month", "week", "day", "today", "hour"]
                property var selection: options_order[currentIndex]
                menu: ContextMenu {
                    Repeater {
                        model: framecombobox.options_order
                        MenuItem {
                            text: framecombobox.available_options[modelData]
                            }
                    }
                }
            }

            TextSwitch {
                id: lineswitch
                enabled: !plot.loading
                checked: true
                text: qsTranslate("sensor-page", "connect measurements")
                description: qsTranslate("sensor-page",
                    "connect data points with a line")
                onCheckedChanged: sensorpage.setplotsource()
            }

            TextSwitch {
                id: zoomswitch
                enabled: !plot.loading
                checked: true
                text: qsTranslate("sensor-page", "zoom")
                description: qsTranslate("sensor-page",
                    "fit time axis to data")
                onCheckedChanged: sensorpage.setplotsource()
            }

        }

        Component.onCompleted: {
            setplotsource()
            framecombobox.currentIndexChanged.connect(setplotsource)
            python.setHandler("latest-sensor-measurement",
                function(measurement) {
                    console.debug("Received latest sensor measurement " +
                        JSON.stringify(measurement))
                    if(measurement.id == sensor.id) {
                        if(sensor.last_time < measurement.last_time) {
                            console.debug(("Time %1 is more up-to-date "
                                + "than our time %2. Updating.")
                                    .arg(measurement.last_time)
                                    .arg(sensor.last_time))
                            sensor.last_time = measurement.last_time
                            sensor.last_value = measurement.last_value
                            // sensor is a dict, member changes do not emit
                            // change signals, so we have to do it by hand
                            sensorChanged()
                        }
                        else {
                            console.debug(("Time %1 is not more up-to-date "
                                + "than our time %2. Not updating.")
                                    .arg(measurement.last_time)
                                    .arg(sensor.last_time))
                        }
                    }
                    else {
                        console.debug("id %1 is not the id of this sensor (%2)"
                            .arg(measurement.id).arg(sensor.id))
                    }
                })
        }

    }

    function setplotsource () {
        plot.setsource("image://python/sensor-measurements?" +
              "frame=%1".arg(framecombobox.selection)
            + "&box_id=%1".arg(box.id)
            + "&sensor_id=%1".arg(sensor.id)
            + "&zoom=%1".arg(zoomswitch.checked ? "yes" : "no")
            + "&line=%1".arg(lineswitch.checked ? "yes" : "no")
            )
    }

}



