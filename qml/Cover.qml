/* -*- coding: utf-8-unix -*-
 *
 * Copyright (C) 2018 Yann Büchau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

CoverBackground {
    id : cover
    property var coverContent:
        app.pageStack.currentPage.coverContent !== undefined ?
        app.pageStack.currentPage.coverContent :
        Qt.createComponent("CoverContent.qml")

    Image {
        id:backgroundimage
        fillMode: Image.PreserveAspectCrop
        anchors.centerIn: parent
        anchors.fill: parent
        clip: true
        source: "../images/cover-background.svg"
        opacity: 0.1
    }

    BusyIndicator {
        id: busyindicator
        visible: app.actions.length > 0
        anchors.centerIn: parent
        size: BusyIndicatorSize.Medium
        running: visible
    }

    CoverLabel {
        id: busylabel
        visible: busyindicator.visible
        anchors {
            bottom: busyindicator.top
            bottomMargin: Theme.paddingSmall
        }
        text: app.latest_action
    }

    CoverLabel {
        id: textlabel
        anchors {
            top: parent.top
            bottom: parent.bottom
        }
        visible: !busylabel.visible && text
        text: coverContent.text || ""
        font.pixelSize: Theme.fontSizeSmall
    }

    CoverActionList {
        id: coverActions
        enabled: coverContent.actionIcon ? true : false

        CoverAction {
            iconSource: coverContent.actionIcon
            onTriggered: {
                coverContent.action()
                }
        }
    }

}

