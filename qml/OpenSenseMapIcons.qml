import QtQuick 2.0

QtObject {
    property var icons : {
        'osem-moisture':"\uf01c",
        'osem-temperature-celsius':"\uf03c",
        'osem-temperature-fahrenheit':"\uf045",
        'osem-thermometer':"\uf055",
        'osem-windspeed':"\uf057",
        'osem-sprinkles':"\uf064",
        'osem-brightness':"\uf06e",
        'osem-barometer':"\uf079",
        'osem-humidity':"\uf07a",
        'osem-not-available':"\uf07b",
        'osem-gauge':"\uf07c",
        'osem-umbrella':"\uf084",
        'osem-clock':"\uf08d",
        'osem-shock':"\uf0c6",
        'osem-fire':"\uf0c7",
        'osem-volume-up':"\uf028",
        'osem-cloud':"\uf0c2",
        'osem-dashboard':"\uf0e4",
        'osem-particulate-matter':"\uf011",
        'osem-signal':"\uf012",
        'osem-microphone':"\uf130",
        'osem-wifi':"\uf1eb",
        'osem-battery':"\uf242",
        'osem-radioactive':"\ue680"
    }

    function get(name) {
        return icons[name] || icons["osem-no-available"]
    }
}
